---
title: Contact
weight: 20
---

## Next YRCS

Please directly contact the organizers at TODO

## Website

For the website, either use the [gitlab project](https://gitlab.com/yrcs/) or mail [Christoph Egger](mailto://christoph.egger@chalmers.se)

