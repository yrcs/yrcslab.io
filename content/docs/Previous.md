---
title: Previous Meetings
weight: 5
---

## Previous Meetings

 -  March 3-5 2025, Konstanz
 -  September 25-27 2024, Darmstadt
 -  March 6-8 2024, Paderborn
 -  September 27-29 2023, Lübeck
 -  March 01-03, 2023, Regensburg
 -  August 25, 26 2022, Karlsruhe
 -  Mai 31th 2021, Wuppertal
 -  October 2020, Darmstadt
 -  March 2020, Karlsruhe
 
