---
title: Mailing List
weight: 10
---

## Mailing List

Young Researcher Crypto Seminar is organized via a [mailinglist](https://www.listserv.dfn.de/sympa/info/yrcs) hosted by "Deutschen Forschungsnetz".
If you are interested head over to the list page or ask someone already involved.
