## Young Researcher Crypto Seminar

The Young Researcher Crypto Seminar (YRCS) is a regular gathering for young researchers based in western-ish Europe working on cryptology. The purpose is to provide a platform for young researchers to discuss research and meet fellow young researchers working on related topics. So far, the definition of “young” seems to be non-faculty. Participants are encouraged to share recently finished research as well as preliminary results.

Established in Spring 2020 by a group of researchers based in Germany, we aim to meet roughly twice a year, with each meeting lasting for one up to three days. The meetings are organised by alternating universities; you can find a list of our previous events and organisers [here](/docs/Previous/).

To join the community, you can simply subscribe to our mailing list. Regularly, you will receive information about the next meetings.

Next edition: September 2025, Karlsruhe
